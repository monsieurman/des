use clap::{App, Arg};
use reqwest;
use scraper::{Html, Selector};
use std::time::Instant;
#[macro_use]
extern crate log;

fn main() {
    let started = Instant::now();
    let matches = init_cli().get_matches();
    let logging_level: log::LevelFilter = match matches.occurrences_of("v") {
        0 => log::LevelFilter::Off,
        1 => log::LevelFilter::Info,
        2 => log::LevelFilter::Debug,
        _ => log::LevelFilter::max(),
    };
    set_logging(logging_level).unwrap();

    let word = matches.value_of("Word").unwrap();
    let synonymes = list_synonymes(word).unwrap();

    let elapsed = started.elapsed();
    info!(
        "Found {} synonyms in {},{}s:",
        synonymes.len(),
        elapsed.as_secs(),
        elapsed.subsec_millis()
    );

    for synonyme in synonymes {
        println!("{}", synonyme);
    }
}

fn set_logging(level: log::LevelFilter) -> Result<(), log::SetLoggerError> {
    fern::Dispatch::new()
        .level(level)
        .chain(std::io::stdout())
        .apply()
}

fn init_cli<'a, 'b>() -> App<'a, 'b> {
    App::new("des")
        .version("0.0.0")
        .about("Fetch synonymes for a given word, in french as of now")
        .author("MrMan")
        .arg(
            Arg::with_name("Word")
                .required(true)
                .help("The word to fetch synonyms for"),
        )
        .arg(
            Arg::with_name("v")
                .short("v")
                .multiple(true)
                .help("Sets the level of verbosity"),
        )
}

fn auto_completion(word: &str) -> Result<Vec<String>, ()> {
    let url = format!("http://crisco.unicaen.fr/des/autocompletion.php?q={}", word);
    let res = request(&url).unwrap();

    let synonymes = res
        .split('\n')
        .filter(|s| !s.is_empty())
        .map(String::from)
        .collect();

    Ok(synonymes)
}

// Could be used to scrap more data than synonymes, eg
fn list_synonymes(word: &str) -> Result<Vec<String>, ()> {
    let url = format!("http://crisco.unicaen.fr/des/synonymes/{}", word);
    let res = request(&url).unwrap();

    let dom = Html::parse_document(res.as_str());
    let selector = Selector::parse("#synonymes>a").unwrap();
    let synonymes: Vec<String> = dom
        .select(&selector)
        .map(|el: scraper::ElementRef| el.text().next().unwrap())
        .map(String::from)
        .collect();

    Ok(synonymes)
}

fn request(url: &str) -> Result<String, reqwest::Error> {
    let client = get_http_client().unwrap();
    let req = client.get(url).send();
    req.unwrap().text()
}

fn get_http_client() -> Result<reqwest::Client, reqwest::Error> {
    let mut builder = reqwest::Client::builder();

    if let Ok(http_proxy) = std::env::var("HTTP_PROXY") {
        builder = builder.proxy(reqwest::Proxy::http(http_proxy.as_str()).unwrap());
        info!("HTTP_PROXY env var found, using: {}", http_proxy);
    }
    if let Ok(https_proxy) = std::env::var("HTTPS_PROXY") {
        builder = builder.proxy(reqwest::Proxy::https(https_proxy.as_str()).unwrap());
        info!("HTTPS_PROXY env var found, using: {}", https_proxy);
    }

    builder.build()
}
